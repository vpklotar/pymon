from PyMon.StatusEnum import Status
from PyMon.ErrorEnum import Error

class iTickable:
    ticktime=1 # Number of ticks between each Tick-function class
    status = Status.Unknown # Status of plugin
    label = "" # Label to display
    name = "iTickable not correctly implemented" # Display name
    error = Error.Ok
    softerror = 1
    harderror = 3
    timeout = 100

    # Tick-function will be called when the TickSpacing has been reaced
    # Time between each tick is specified on the ticker itself
    def Tick(self):
        print("Tick-function not properly implemented!")

    # Will return a human readable display of the status of this plugin
    def GetStatusLabel(self):
        if (self.error == Error.Softerror):
            ErrorLabel = "\033[33m" + self.error.name + "\x1b[0m"
        elif (self.error == Error.Harderror):
            ErrorLabel = "\033[31m" + self.error.name + "\x1b[0m"
        elif (self.error == Error.Ok):
            ErrorLabel = "\033[32m" + self.error.name + "\x1b[0m"
        else:
            ErrorLabel = self.error.name
        if (self.label == ""):
            return self.status.name  + " (" + ErrorLabel + ")"
        else:
            return self.label  + " (" + ErrorLabel + ")"
