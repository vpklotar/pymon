# This file contains default values and is a container for other values
# This class get sent to Plugins (tests)

from PyMon.Host import Host

class Group:
    ticktime = 1
    hosts = None
    name = None
    softerror = 1
    harderror = 3

    # Loop through all the variables set on the group and set them
    def __init__(self, data=""):
        self.hosts = {}
        self.config = data

        if ("name" in self.config):
            self.name = self.config["name"]
        else:
            print("Name not set on group, exiting!")
            exit()
        if ("ticktime" in self.config):
            self.ticktime = float(self.config["ticktime"])

    # Create all hosts from config file
    def InitHosts(self):
        hosts = self.config["hosts"]
        for key in hosts:
            host = Host(key)
            host.group = self
            host.ticktime = float(key["ticktime"]) if "ticktime" in key else host.group.ticktime
            host.softerror = int(key["softerror"]) if "softerror" in key else host.group.softerror
            host.harderror = float(key["harderror"]) if "harderror" in key else host.group.harderror
            host.InitPlugins()
            self.hosts[key["name"]] = host
