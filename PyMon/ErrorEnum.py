from enum import Enum

class Error(Enum):
    Ok = 0
    Softerror = 1
    Harderror = 2
