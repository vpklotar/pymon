from enum import Enum
class Status(Enum):
    Ok = 0
    Warn = 1
    Nok = 2
    Unknown = 3
