# This file contains default values and is a container for other values
# This class get sent to Plugins (tests)

import _thread
from time import sleep
from datetime import datetime

from PyMon.StatusEnum import Status
from PyMon.ErrorEnum import Error

class Host:
    group = "default"

    # Loop through all the variables set on the host and set them
    def __init__(self, data=""):
        for key in data:
            setattr(self, key, data[key])

    def InitPlugins(self):
        for key in self.plugins:
            # Dynamicly load plugin if needed
            imp = __import__("Plugins." + str(key), fromlist=[str(key)])
            module = getattr(imp, key)
            # Init the plugin
            plugin = module(self, self.plugins[key])

            plugin.ticktime = float(self.plugins[key]["ticktime"]) if "ticktime" in self.plugins[key] else self.ticktime
            plugin.softerror = float(self.plugins[key]["softerror"]) if "softerror" in self.plugins[key] else self.softerror
            plugin.harderror = float(self.plugins[key]["harderror"]) if "harderror" in self.plugins[key] else self.harderror

            self.plugins[key] = plugin
            # Start the plugin
            _thread.start_new_thread(self.TickHandler, (plugin,))

    def TickHandler(self, plugin):
        TickSienceError = 0
        while True:
            plugin.Tick()

            # Check SoftError status
            if (plugin.status != Status.Ok):
                TickSienceError += 1
                if(TickSienceError >= plugin.softerror and plugin.error != Error.Harderror):
                    plugin.error = Error.Softerror
                # Check HardError status
                if(TickSienceError >= plugin.harderror):
                    plugin.error = Error.Harderror

            if(plugin.status == Status.Ok):
                TickSienceError = 0
                plugin.error = Error.Ok
            
            sleep(plugin.ticktime)
