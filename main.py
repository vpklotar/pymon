#!/usr/bin/python3

from time import sleep

import sys
import os
import json
from PyMon.Group import Group
from PyMon.Host import Host
from os.path import isfile
from PyMon.StatusEnum import Status

class Main:
    config = None # Config
    groups = {} # Dictionary of groups
    hosts = {} # Dictionary of groups
    args = {} # All arguments

    def __init__(self, args):
        self.ReadArgs(args)
        self.InitConfig()
        self.InitGroups()
        self.DisplayStatus()

    def DisplayStatus(self):
        while True:
            os.system("clear")
            for group in self.groups:
                print(group + ":")
                for host in self.groups[group].hosts:
                    h = self.groups[group].hosts[host]
                    print("\t"+host + ":")
                    for plugin in h.plugins:
                        print("\t\t" + h.plugins[plugin].name + "\t" + h.plugins[plugin].GetStatusLabel())
            sleep(0.5)

    # Read the config file and return a config object
    def InitConfig(self):
        if (isfile(self.args["config"])):
            print("Using config file '" + self.args["config"] + "'")
            fh = open(self.args["config"], "r")
            content = fh.read()
            fh.close()
            self.config = json.loads(content)
        else:
            print("\033[31mConfig file '" + self.args["config"] + "' does not exist!\x1b[0m")
            exit(1)

    # Create all groups configured in the config
    def InitGroups(self):
        groups = self.config["groups"] # Groups section in config file
        for group in groups:
            gp = Group(group)
            gp.InitHosts()
            self.groups[gp.name] = gp

    def ReadArgs(self, args):
        if "-c" in args:
            index = args.index("-c")+1
            if(index < len(args)):
                self.args["config"] = args[index]
            else:
                print("Missing path for config for argument '-c'")
                exit(1)
        else:
            self.args["config"] = "config.json"


if (__name__ == "__main__"):
    Main(sys.argv[1:])

sleep(10000)
