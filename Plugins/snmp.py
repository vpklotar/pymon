# SNMP module

from PyMon.StatusEnum import Status
from PyMon.Interfaces.iTickable import iTickable
from subprocess import Popen, PIPE
import re

class snmp(iTickable):
    version = None # SNMP Version to use
    mib = None # What mib to get the value from
    community = None # What SNMP community to use
    address = None # What host to query for SNMP values
    name="SNMP" # Display name
    display = ".+" # Regex to filter out the information wanted in the reply

    def __init__(self, host, conf):
        if ("mib" not in conf):
            print("snmp module missing mib-value!")
            exit()
        if ("version" not in conf):
            print("snmp module missing version-value!")
            exit()
        if ("community" not in conf):
            print("snmp module missing community-value!")
            exit()
        if ("name" in conf):
            self.name = conf["name"]
        if ("display" in conf):
            self.display = conf["display"]

        self.version = conf["version"]
        self.community = conf["community"]
        self.mib = conf["mib"]
        self.address = host.address

    def Tick(self):
        p = Popen(["snmpget", "-v"+str(self.version), "-c"+str(self.community), str(self.address), str(self.mib)], stdout=PIPE, stderr=PIPE)
        output = p.communicate()[0]
        output = output.decode("UTF-8")
        output = re.findall(self.display, output)[0]
        self.status = Status.Ok
        self.label = output
