# Ping check plugin for PyMon

from PyMon.Interfaces.iTickable import iTickable
from PyMon.StatusEnum import Status
import subprocess

class ping(iTickable):
    address = ""
    timeout = 3
    name="Ping"

    def __init__(self, host, data):
        if ("timeout" in data):
            self.timeout = float(data["timeout"])
        if ("name" in data):
            self.name = str(data["name"])
        self.address = host.address

    def Tick(self):
        try:
            p = subprocess.check_output(["ping", "-c1", self.address], stderr=subprocess.STDOUT, timeout=self.timeout)
            self.status = Status.Ok
        except subprocess.TimeoutExpired:
            self.status = Status.Nok
        except subprocess.CalledProcessError:
            self.status = Status.Nok
